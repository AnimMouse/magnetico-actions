# Magnetico on GitHub Actions
[Magnetico](https://github.com/boramalper/magnetico) BitTorrent DHT search engine running on GitHub Actions

Proof of concept magneticod and magneticow autonomous (self-hosted) BitTorrent DHT search engine suite running on GitHub Actions.